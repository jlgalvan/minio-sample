# Minio Sample Cluster

Ejemplo de construcción de un clúster de almacenamiento Object Storage con Min.io.  

El ejemplo se basa en la siguiente arquitectura:  

![Arquitectura del clúster](./assets/minio-sample-scheme.png "Arquitectura del clúster")

La infraestructura necesaria está compuesta por:

- **Fichero [docker-compose.yml](docker-compose.yml)**: Despliega un clúster con 4 nodos de Min.io junto con un nodo de Nginx que servirá de balanceador de carga para distribuir los accesos. En caso de que uno de nuestros nodos no esté disponible Nginx se encargará de redireccionar nuestra petición a un nodo que si esté prestando servicio.
- **Fichero [nginx.conf](nginx.conf):** que contiene la configuración Nginx.
 
Pasos para nuestra prueba:

1. Para iniciar la infraestructura ejecutar:  `docker-compose up -d`

2. Podemos probar que todo está funcionando correctamente accediendo desde nuestro navegador web a la dirección `http://localhost:9000/minio/login`. Puedes acceder con los credenciales configurados (*minio/minio123*).

3. Para administrar el clúster puedes utilizar el cliente que podrás descargar desde https://min.io/download.
Una vez instalado puedes configurar la conexión al cluster con el comando:

`mc alias set minio http://localhost:9000 minio minio123`

4. Para combrobar el estado de nuestro cluster ejecutamos desde la consola:

`mc admin info minio`

Como salida obtenemos que todos nuestros nodos están operativos.

![Salida mc admin info](./assets/minio-admin-ok.png)

5. Simulamos la caída de dos de nuestros nodos, por ejemplo podemos detener un par de contenedores lo cual debe producir la indisponibilidad de 2 nodos del clúster y 4 discos de almacenamiento.

`docker stop minio1 minio3`

Si ejecutamos ahora nuestro comando para consultar el estado del clúster vemos la siguiente salida:

![Salida mc admin info](./assets/minio-admin-fail.png)

6. Si accedemos a la aplicación web de nuestro clúster (http://localhost:9000) veremos que aún teniendo la mitad de discos disponibles podemos acceder a la información, esto es porque por defecto configura Erasure coding para disponer de una paridad N/2 donde N=nº de discos disponibles en el clúster. En este estado el cluster seguirá funcionando frente a lecturas, pero no podremos realizar escrituras hasta restaurar su situación. La configuración de paridad se puede establecer a nuestras necesidades, si bien menor paridad puede significa asumir mayores riesgos.

Una mayor descripción en el post: https://jlgalvan-rojas.medium.com/datalake-object-storage-con-min-io-9a8ae070a17d
